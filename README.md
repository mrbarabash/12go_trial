# 12go Asia Trial Case
12go Asia Trial Case is the implementation for the task described [here](https://open.kattis.com/problems/alldifferentdirections).

## Code Requirements
- PHP 7.2+

## Usage

As provided in task you should have a text input like this:

```php
$input = <<<INPUT
3
87.342 34.30 start 0 walk 10.0
2.6762 75.2811 start -45.0 walk 40 turn 40.0 walk 60
58.518 93.508 start 270 walk 50 turn 90 walk 40 turn 13 walk 5
2
30 40 start 90 walk 5
40 50 start 180 walk 10 turn 90 walk 5
0
INPUT;
```

### 1. Create an instance of InstructionManager

```php
use App\Manager\InstructionManager;

$instructionManager = new InstructionManager($input);
```

### 2. Process instructions

```php
$instructionManager->processInstructions();
```

### 3. Retrieve result output

```php
$output = $instructionManager->getOutput();
```
`$output` is an array containing lines with result in described format. For our input it would be the following:

```php
['97.1547 40.2334 7.63097', '30 45 0']
```

## Testing
Run
```ssh
$ vendor/bin/phpunit tests
```

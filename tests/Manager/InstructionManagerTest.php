<?php

namespace Test\Manager;

use App\Manager\InstructionManager;
use Test\BaseTest;

class InstructionManagerTest extends BaseTest
{
    const EPSILON = 0.1;

    /**
     * @group unit
     */
    public function testOutput()
    {
        $input = <<<INPUT
3
87.342 34.30 start 0 walk 10.0
2.6762 75.2811 start -45.0 walk 40 turn 40.0 walk 60
58.518 93.508 start 270 walk 50 turn 90 walk 40 turn 13 walk 5
2
30 40 start 90 walk 5
40 50 start 180 walk 10 turn 90 walk 5
0
INPUT;

        $instructionManager = new InstructionManager($input);
        $instructionManager->processInstructions();
        $output = $instructionManager->getOutput();

        static::assertCount(2, $output, 'Output results count should be exactly 2');

        [$x, $y, $distance] = explode(' ', $output[0]);
        static::assertEqualFloat(97.1547, $x, self::EPSILON);
        static::assertEqualFloat(40.2334, $y, self::EPSILON);
        static::assertEqualFloat(7.63097, $distance, self::EPSILON);

        [$x, $y, $distance] = explode(' ', $output[1]);
        static::assertEqualFloat(30, $x, self::EPSILON);
        static::assertEqualFloat(45, $y, self::EPSILON);
        static::assertEqualFloat(0, $distance, self::EPSILON);
    }
}

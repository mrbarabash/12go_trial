<?php

namespace Test;

use PHPUnit\Framework\TestCase;

abstract class BaseTest extends TestCase
{
    /**
     * @param float $expected
     * @param float $value
     * @param float $precision
     */
    protected static function assertEqualFloat(float $expected, float $value, float $precision)
    {
        self::assertTrue(
            abs($expected - $value) < $precision,
            sprintf('The value "%f" is not equal to "%f".', $value, $expected)
        );
    }
}

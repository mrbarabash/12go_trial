<?php

namespace App\Contracts;

use App\Model\Instruction;

class InstructionContract
{
    /**
     * @param Instruction $instruction
     *
     * @throws \LogicException
     */
    public static function ensurePeopleDirection(Instruction $instruction): void
    {
        if (!$instruction->isPeopleDirection()) {
            throw new \LogicException(
                sprintf('Instruction "%s" is not a people\'s direction.', $instruction->getText())
            );
        }
    }

    /**
     * @param Instruction $instruction
     *
     * @throws \LogicException
     */
    public static function ensureTestCaseStart(Instruction $instruction): void
    {
        if (!$instruction->isTestCaseStart()) {
            throw new \LogicException(
                sprintf('Instruction "%s" is not a test case start / end.', $instruction->getText())
            );
        }
    }
}

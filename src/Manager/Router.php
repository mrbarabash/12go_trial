<?php

namespace App\Manager;

use App\Model\Pointer;

class Router
{
    /**
     * @var Pointer
     */
    private $point;

    /**
     * @param Pointer $startPoint
     */
    public function __construct(Pointer $startPoint)
    {
        $this->point = $startPoint;
    }

    /**
     * @param float $angle
     *
     * @return static
     */
    public function start(float $angle): self
    {
        $this->point->setAngle($angle);

        return $this;
    }

    /**
     * @param float $steps
     *
     * @return static
     */
    public function walk(float $steps): self
    {
        $x = $steps * cos(deg2rad($this->point->getAngle()));
        $y = $steps * sin(deg2rad($this->point->getAngle()));

        $this->getPoint()->setX($this->getPoint()->getX() + $x);
        $this->getPoint()->setY($this->getPoint()->getY() + $y);

        return $this;
    }

    /**
     * @param float $angle
     *
     * @return static
     */
    public function turn(float $angle): self
    {
        $this->getPoint()->setAngle($this->getPoint()->getAngle() + $angle);

        return $this;
    }

    /**
     * @return Pointer
     */
    public function getPoint(): Pointer
    {
        return $this->point;
    }
}

<?php

namespace App\Manager;

use App\Model\Pointer;

class PointerManager
{
    /**
     * @param Pointer $point1
     * @param Pointer $point2
     *
     * @return float
     */
    public function getDistanceBetweenPoints(Pointer $point1, Pointer $point2): float
    {
        return sqrt(pow($point1->getX() - $point2->getX(), 2) + pow($point1->getY() - $point2->getY(), 2));
    }

    /**
     * @param Pointer $averagePoint
     * @param Pointer ...$points
     *
     * @return float
     */
    public function getWorstDistance(Pointer $averagePoint, Pointer ...$points): float
    {
        $distances = [];
        foreach ($points as $point) {
            $distances[] = $this->getDistanceBetweenPoints($averagePoint, $point);
        }

        return max($distances);
    }

    /**
     * @param Pointer ...$points
     *
     * @return Pointer
     */
    public function getAveragePoint(Pointer ...$points): Pointer
    {
        $pointsCount = count($points);
        if (!$pointsCount) {
            throw new \InvalidArgumentException('Please provide points to calculate an average point coordinates.');
        }
        $sumX = $sumY = 0;
        foreach ($points as $point) {
            $sumX += $point->getX();
            $sumY += $point->getY();
        }

        return (new Pointer())
            ->setX($sumX / $pointsCount)
            ->setY($sumY / $pointsCount)
        ;
    }
}

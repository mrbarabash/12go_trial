<?php

namespace App\Manager;

use App\Contracts\InstructionContract;
use App\Model\Instruction;
use App\Model\Pointer;

class InstructionManager
{
    /**
     * @var PointerManager
     */
    private $pointerManager;

    /**
     * @var Instruction[]
     */
    private $instructions = [];

    /**
     * @var array
     */
    private $testCasesPoints = [];

    /**
     * @param string $text
     */
    public function __construct(string $text)
    {
        $this->pointerManager = new PointerManager();

        // TODO: Iterator
        $this->instructions = array_map(function (string $text) {
            return (new Instruction())
                ->setText($text)
            ;
        }, explode(PHP_EOL, $text));
    }

    /**
     * @return Instruction[]
     */
    public function getInstructions(): array
    {
        return $this->instructions;
    }

    public function processInstructions(): void
    {
        foreach ($this->instructions as $instruction) {
            if ($instruction->isEndInstruction()) {
                $this->instructions = [];
                break;
            }
            if ($instruction->isTestCaseStart()) {
                $directionsCount = $instruction->getPeopleDirectionsCount();

                // Remove test case start instruction
                array_shift($this->instructions);

                // Slice N directions
                $directions = array_splice($this->instructions, 0, $directionsCount);

                // Transform directions into points
                $points = [];
                foreach ($directions as $direction) {
                    $points[] = $this->processPeopleDirection($direction);
                }

                // Save points for the output
                $this->testCasesPoints[] = $points;
            }
        }
    }

    /**
     * @param Instruction $instruction
     *
     * @return Pointer
     */
    public function processPeopleDirection(Instruction $instruction): Pointer
    {
        InstructionContract::ensurePeopleDirection($instruction);

        $router = new Router($instruction->getStartPoint());
        foreach ($instruction->getActions() as $instructionAction) {
            $this->processInstructionAction($router, $instructionAction);
        }

        return $router->getPoint();
    }

    /**
     * @return array Array of strings: x y distance
     */
    public function getOutput(): array
    {
        if (!$this->testCasesPoints) {
            throw new \LogicException('Process instructions first.');
        }

        $output = [];
        foreach ($this->testCasesPoints as $testCasePoints) {
            $averagePoint = $this->pointerManager->getAveragePoint(...$testCasePoints);
            $worstDistance = $this->pointerManager->getWorstDistance($averagePoint, ...$testCasePoints);

            $output[] = sprintf(
                '%s %s %s',
                round($averagePoint->getX(), 4),
                round($averagePoint->getY(), 4),
                round($worstDistance, 5)
            );
        }

        return $output;
    }

    /**
     * @param Router $router
     * @param string $action
     *
     * @throws \LogicException
     */
    private function processInstructionAction(Router $router, string $action): void
    {
        [$actionType, $actionValue] = explode(' ', $action);

        if (!method_exists($router, $actionType)) {
            throw new \LogicException(sprintf('Action "%s" is not implemented yet in Router.', $actionType));
        }

        $router->{$actionType}($actionValue);
    }
}

<?php

namespace App\Model;

class Pointer
{
    /**
     * @var float
     */
    private $x;

    /**
     * @var float
     */
    private $y;

    /**
     * @var float
     */
    private $angle = 0.0;

    /**
     * @return float
     */
    public function getX(): float
    {
        return $this->x;
    }

    /**
     * @param float $x
     *
     * @return static
     */
    public function setX(float $x): self
    {
        $this->x = $x;

        return $this;
    }

    /**
     * @return float
     */
    public function getY(): float
    {
        return $this->y;
    }

    /**
     * @param float $y
     *
     * @return static
     */
    public function setY(float $y): self
    {
        $this->y = $y;

        return $this;
    }

    /**
     * @return float
     */
    public function getAngle(): float
    {
        return $this->angle;
    }

    /**
     * @param float $angle
     *
     * @return static
     */
    public function setAngle(float $angle): self
    {
        $this->angle = $angle;

        return $this;
    }
}

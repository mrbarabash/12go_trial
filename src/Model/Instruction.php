<?php

namespace App\Model;

use App\Contracts\InstructionContract;

class Instruction
{
    const INSTRUCTION_END = '0';
    const INSTRUCTION_TEST_CASE_START_PATTERN = '~^\d{1,2}$~';

    const INSTRUCTION_COORDINATES_POSITION = 0;
    const INSTRUCTION_START_POSITION = 1;
    const INSTRUCTION_ACTIONS_POSITION = 2;

    /**
     * Array of instruction parts such as coordinates, start, walks and turns.
     *
     * @internal
     *
     * @var array
     */
    private $instructionParts = [];

    /**
     * @return string
     */
    public function getText(): string
    {
        return implode(' ', $this->getInstructionParts());
    }

    /**
     * @param string $text
     *
     * @return static
     */
    public function setText(string $text): self
    {
        $text = trim($text);

        // In case of test case start or instruction end we just set one part.
        if (preg_match(self::INSTRUCTION_TEST_CASE_START_PATTERN, $text)) {
            $this->setInstructionParts([$text]);
        } else {
            preg_match_all('~(?P<parts>\S+\s\S+)~', $text, $result);
            $this->setInstructionParts(array_key_exists('parts', $result) ? $result['parts'] : []);
        }

        return $this;
    }

    /**
     * "Input ends when n is zero."
     *
     * @return bool
     */
    public function isEndInstruction(): bool
    {
        $instructionParts = $this->getInstructionParts();

        return count($instructionParts) === 1
            && reset($instructionParts) === self::INSTRUCTION_END;
    }

    /**
     * "Each test case starts with an integer 1≤n≤20."
     *
     * @return bool
     */
    public function isTestCaseStart(): bool
    {
        $instructionParts = $this->getInstructionParts();

        return count($instructionParts) === 1
            && preg_match(self::INSTRUCTION_TEST_CASE_START_PATTERN, reset($instructionParts));
    }

    /**
     * "Each person’s directions contain at most 25 instructions."
     *
     * @return bool
     */
    public function isPeopleDirection(): bool
    {
        return count($this->getInstructionParts()) > 1;
    }

    /**
     * @return Pointer
     */
    public function getStartPoint(): Pointer
    {
        InstructionContract::ensurePeopleDirection($this);

        [$x, $y] = $this->getCoordinates();
        $angle = $this->getStartAngle();

        return (new Pointer())
            ->setX($x)
            ->setY($y)
            ->setAngle($angle)
        ;
    }

    /**
     * @return array [x, y]
     */
    public function getCoordinates(): array
    {
        InstructionContract::ensurePeopleDirection($this);
        $coordinates = array_slice($this->getInstructionParts(), self::INSTRUCTION_COORDINATES_POSITION, 1);

        return explode(' ', reset($coordinates));
    }

    /**
     * @return float
     */
    public function getStartAngle(): float
    {
        InstructionContract::ensurePeopleDirection($this);
        $startAngle = array_slice($this->getInstructionParts(), self::INSTRUCTION_START_POSITION, 1);

        return explode(' ', reset($startAngle))[1];
    }

    /**
     * @return array
     */
    public function getActions(): array
    {
        InstructionContract::ensurePeopleDirection($this);

        return array_slice($this->getInstructionParts(), self::INSTRUCTION_ACTIONS_POSITION);
    }

    /**
     * @return int
     */
    public function getPeopleDirectionsCount(): int
    {
        InstructionContract::ensureTestCaseStart($this);

        return (int) $this->getInstructionParts()[0];
    }

    /**
     * @return array
     */
    private function getInstructionParts(): array
    {
        return $this->instructionParts;
    }

    /**
     * @param array $instructionParts
     *
     * @return static
     */
    private function setInstructionParts(array $instructionParts): self
    {
        $this->instructionParts = $instructionParts;

        return $this;
    }
}
